% TiLo 2020 - P5 - Ex1 - PDA

% PDA for M_palin_ab
% A valid word is a palindrom over symbols a and b
% M(Z, E, G, d, Z0, #)

% Z
state(z).

% E
sigma(a).
sigma(b).

% G
gamma(a).
gamma(b).
gamma(s).

% Z0
start(z).

% #
keller(s).

% d (z, a, s, z', s1)
delta(z, a, a, z, []).
delta(z, b, b, z, []).
delta(z, nix, s, z, []).
delta(z, nix, s, z, [a]).
delta(z, nix, s, z, [b]).
delta(z, nix, s, z, [a,s,a]).
delta(z, nix, s, z, [b,s,b]).

sigma_stern([]).                            % base for empty word
sigma_stern([X|Xs]) :-                      %
    sigma(X), sigma_stern(Xs).              % recursive split for non empty words

% single step relation
% es(z, aw, gs, z', w, s's) |- (z, a, g, z', s') in delta
es(Zact, [A|Ws], [G|S0s], Znew, Ws, S1s) :- % single step relation with input reduction and
    state(Zact),                            % validate states
    state(Znew),
    sigma(A),                               % only symbols of E allowed in input word
    gamma(G),                               % symbol in stack must be in gamma
    delta(Zact, A, G, Znew, S),             % calculate matching delta
    append(S, S0s, S1s).                    % edit and return stack appending delta result

es(Zact, [A|Ws], [G|S0s], Znew, [A|Ws], S1s):-  % single step relation, keep input
    state(Zact),                            % validate states
    state(Znew),
    sigma(A),                               % only symbols of E allowed in input word
    gamma(G),                               % symbol in stack must be in gamma
    delta(Zact, nix, G, Znew, S),           % calculate matching delta
    append(S, S0s, S1s).                    % edit stack appending delta result


% transitive closure for single step relations
es_plus(Z, [], [], Z, [], []).              % empty word and stack -> return true
es_plus(Z0, W0s, S0s, Z1, W1s, S1s):-       % word and stack not empty
    es(Z0, W0s, S0s, Z2, W2s, S2s),         % call single step relation, calculating first symbol
    es_plus(Z2, W2s, S2s, Z1, W1s, S1s).    % recursive call with single step output as arguments

% calculate language over PDA
lvonM(Ws):-
    sigma_stern(Ws),                        % validate symbols in input
    start(Z),                               % find valid start state
    state(E),                               % choose valid end state
    keller(Ss),                             % init stackbase
    es_plus(Z, Ws, [Ss], E, [], []).        % call transitive closure
